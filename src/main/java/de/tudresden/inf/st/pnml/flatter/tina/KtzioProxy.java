package de.tudresden.inf.st.pnml.flatter.tina;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class KtzioProxy extends AbstractTinaProxy {

    public void convertBinaryToText(String inputPath, String outputPath) throws IOException, InterruptedException {

        ProcessBuilder ktzioProcessBuilder = new ProcessBuilder();

        if (!isWindows) {

            File file = new File(outputPath);
            if (!file.exists()) {
                file.getParentFile().mkdirs();
            }

            List<String> commandParts = new ArrayList<>();
            if (homeDirectory.contains("pnml-relast-flattener")) {

                commandParts.add(homeDirectory + "/libs/tina-3.7.0/bin/ktzio");
            } else {

                commandParts.add(homeDirectory + "/pnml-relast-flatter/libs/tina-3.7.0/bin/ktzio");
            }

            commandParts.add("-KTZ");
            commandParts.add("-txt");
            commandParts.add(inputPath);
            commandParts.add(outputPath);

            ktzioProcessBuilder.command(commandParts);
            Process p = ktzioProcessBuilder.start();
            p.waitFor();
        } else {
            logger.error("Windows is currently not supported. Exiting ...");
            return;
        }
    }
}
