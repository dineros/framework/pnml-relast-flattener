package de.tudresden.inf.st.pnml.flatter.template;

import de.tudresden.inf.st.pnml.jastadd.model.Name;
import de.tudresden.inf.st.pnml.jastadd.model.PnObject;

public abstract class PnmlTemplate {

    public static final String TEMPLATES_PATH = "templates/";
    public static final String ELEMENTS_PATH = "elements/";

    protected static void updatePnObjectIdAndName(PnObject po, String idSuffix){

        String oldId = po.getId();
        po.setId(oldId + "-" + idSuffix);
        Name name = new Name();
        name.setText(oldId + "-" + idSuffix);
        po.setName(name);
    }
}
