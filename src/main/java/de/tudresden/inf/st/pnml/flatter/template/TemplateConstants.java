package de.tudresden.inf.st.pnml.flatter.template;

public final class TemplateConstants {

    // topic publisher
    public static final String PUBLISHER_CAPACITY_PLACE = "PublisherCapacityPlace";
    public static final String PUBLISHER_CONNECTOR_PLACE = "PublisherConnectorPlace";
    public static final String PUBLISHER_INPUT_TRANSITION = "PublisherInputTransition";
    public static final String PUBLISHER_OVERFLOW_TRANSITION = "PublisherOverflowTransition";
    public static final String PUBLISHER_OUTPUT_TRANSITION = "PublisherOutputTransition";
    public static final String PLACE_TOPIC_PUBLISHER_REF = "PublisherPlaceRef";
    public static final String PLACE_TOPIC_PUBLISHER_CHANNEL_REF = "PublisherChannelPlaceRef";
    public static final String TRANSITION_TOPIC_DISPATCHER_CHANNEL_REF = "DispatcherChannelTransitionRef";
    public static final String PLACE_TOPIC_CALLBACK_REF = "CallbackRefPlace";
    public static final String TRANSITION_TOPIC_CALLBACK_INPUT_REF = "CallbackInputRefTransition";

    // topic dispatcher
    public static final String DISPATCHER_INPUT_TRANSITION = "DispatcherInputTransition";
    public static final String DISPATCHER_OUTPUT_TRANSITION = "DispatcherOutputTransition";
    public static final String DISPATCHER_INPUT_PLACE = "DispatcherInputPlace";
    public static final String DISPATCHER_OVERFLOW_TRANSITION = "DispatcherOverflowTransition";
    public static final String DISPATCHER_CAPACITY_PLACE = "DispatcherCapacityPlace";
    public static final String DISPATCHER_CONNECTOR_PLACE = "DispatcherConnectorPlace";

    // topic callback
    public static final String CALLBACK_INPUT_PLACE = "CallbackInputPlace";
    public static final String CALLBACK_OUTPUT_TRANSITION = "CallbackOutputTransition";
    public static final String CALLBACK_OVERFLOW_TRANSITION = "CallbackOverflowTransition";
    public static final String CALLBACK_CAPACITY_PLACE = "CallbackCapacityPlace";
    public static final String CALLBACK_CONNECTOR_PLACE = "CallbackConnectorPlace";

    // template pages
    public static final String PUBLISHER_TEMPLATE_PAGE = "PublisherTemplatePage";
    public static final String DISPATCHER_TEMPLATE_PAGE = "TopicSubDispatcherTemplatePage";
    public static final String CALLBACK_QUEUE_TEMPLATE_PAGE = "TopicSubCallbackTemplatePage";
    public static final String SERVICE_TEMPLATE_PAGE = "ServiceConnectionTemplate";
    public static final String SERVICE_QUEUE_TEMPLATE_PAGE = "ServiceQueueTemplatePage";

    // channel elements
    public static final String CHANNEL_TOPIC_PLACE = "ChannelConnectorPlace";
    public static final String CHANNEL_TOPIC_TRANSITION = "ChannelConnectorTransition";
    public static final String CHANNEL_TOPIC_PAGE_PREFIX = "ChannelTopicPage";
    public static final String CHANNEL_SERVICE_PAGE_PREFIX = "CHANNEL-SERVICE-PAGE-";

    // signal elements
    public static final String INPUT_SIGNAL_PLACE_TRUE = "InputSignalTruePlace";
    public static final String INPUT_SIGNAL_PLACE_FALSE = "InputSignalFalsePlace";
    public static final String INPUT_SIGNAL_TRANSITION_TO_TRUE = "InputSignalToTrueTransition";
    public static final String INPUT_SIGNAL_TRANSITION_TO_FALSE = "InputSignalToFalseTransition";
    public static final String INPUT_SIGNAL_PAGE_PREFIX = "InputSignalPage";
    public static final String PD_PLACE_PREFIX = "PD-";

    // services
    // S3
    public static final String SERVICE_ACTIVE_PLACE = "ServiceCallMultiActivePlace";
    public static final String SERVICE_INACTIVE_PLACE = "ServiceCallMultiInactivePlace";
    // S2
    public static final String SERVICE_FLAG_PLACE = "ServiceCallMultiFlagPlace";
    public static final String SERVICE_CALL_ENTRY_TRANS = "ServiceCallMultiEntryTransition";
    public static final String SERVICE_CALL_EXIT_TRANS = "ServiceCallMultiExitTransition";
    public static final String SERVICE_CALL_M_REF_PLACE = "ServiceCallMultiCallRefPlace";
    public static final String SERVICE_RESP_M_REF_PLACE = "ServiceCallMultiRespRefPlace";
    public static final String SERVICE_INACTIVE_M_REF_PLACE = "ServiceCallMultiInactiveRefPlace";
    public static final String SERVICE_ACTIVE_M_REF_PLACE = "ServiceCallMultiActiveRefPlace";
    public static final String SERVICE_ENTRY_M_REF_PLACE = "ServiceCallMultiEntryRefPlace";
    public static final String SERVICE_EXIT_M_REF_PLACE = "ServiceCallMultiExitRefPlace";
    // S1
    public static final String SERVICE_CON_TOGGLE_PLACE = "ServiceTogglePlace";
    public static final String SERVICE_CON_CLIENT_CALL_CON_PLACE = "ServiceClientCallConnectPlace";
    public static final String SERVICE_CON_CLIENT_RESP_CON_PLACE = "ServiceClientRespConnectPlace";
    public static final String SERVICE_CON_CLIENT_CALL_PLACE = "ServiceClientCallPlace";
    public static final String SERVICE_CON_CLIENT_RESP_PLACE = "ServiceClientRespPlace";
    public static final String SERVICE_CON_CALL_CON_TRANS = "ServiceClientCallConnectTransition";
    public static final String SERVICE_CON_RESP_CON_TRANS = "ServiceClientRespConnectTransition";
    public static final String SERVICE_CON_CALL_TRANS = "ServiceClientCallTransition";
    public static final String SERVICE_CON_RESP_TRANS = "ServiceClientRespTransition";
    public static final String SERVICE_CAN_CALL_REF_PLACE = "ServiceCallRefPlace";
    public static final String SERVICE_CAN_RESP_REF_PLACE = "ServiceRespRefPlace";

}

