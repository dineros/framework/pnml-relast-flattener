package de.tudresden.inf.st.pnml.flatter.analysis;

import beaver.Parser;
import de.tudresden.inf.st.pnml.flatter.tina.TinaProxy;
import de.tudresden.inf.st.pnml.jastadd.model.*;
import de.tudresden.inf.st.pnml.jastadd.scanner.TinaMarkingScanner;
import de.tudresden.inf.st.pnml.jastadd.scanner.TinaReachabilityScanner;
import de.tudresden.inf.st.pnml.jastadd.tinaParser.TinaMarkingParser;
import de.tudresden.inf.st.pnml.jastadd.tinaParser.TinaReachabilityParser;
import org.apache.commons.lang3.StringUtils;

import java.io.*;
import java.util.UUID;

public class TinaUtils {

    public static int checkBoundednessOfPlace(String placeId, String inputPath, String pageId) throws IOException, Parser.Exception {

        TinaProxy tinaProxy = new TinaProxy();
        String tinaTargetPath = System.getProperty("user.dir") + "/temp/post/" + "tina-result-" + pageId + "-" + UUID.randomUUID().toString() + ".txt";

        String[] config = {"-NET", "-stats"};
        tinaProxy.analyzePetriNet(inputPath, tinaTargetPath, config);

        // clip and parse result
        StringBuilder clippedMarkingData = new StringBuilder();

        BufferedReader br = new BufferedReader(new FileReader(new File(tinaTargetPath)));
        String line;
        while ((line = br.readLine()) != null) {
            if (line.contains(":") && !line.contains("MARKINGS")
                    && !line.contains("REACHABILITY")
                    && !line.contains("unbounded places")) {
                clippedMarkingData.append(line).append("\n");
            }
        }

        clippedMarkingData = new StringBuilder(StringUtils.chomp(clippedMarkingData.toString()));

        if (clippedMarkingData.length() == 0 ) {
            System.err.println("Could not construct marking graph. Reason: no data.");
            return -1;
        }

        // construct a graph
        TinaMarking tm = parseTinaMarking(clippedMarkingData.toString());
        // printTinaMarking(tm);

        int maxMarking = -1;

        for(NetMarking nm : tm.getNetMarkingList()) {
            for (PlaceMarking pm : nm.getPlaceMarkingList()) {
                if(pm.getPlaceName().equals(placeId)){
                    if (pm.isMultMarking() && pm.asMultMarking().getMultiplier() > maxMarking) {
                        maxMarking = pm.asMultMarking().getMultiplier();
                    } else {
                        if(maxMarking < 1){
                            maxMarking = 1;
                        }
                    }
                }
            }
        }

        return maxMarking;
    }

    public static TinaMarking parseTinaMarking(String data) throws IOException, Parser.Exception {

        StringReader input = new StringReader(data);

        TinaMarkingScanner tms = new TinaMarkingScanner(input);
        TinaMarkingParser tmp = new TinaMarkingParser();

        return (TinaMarking) tmp.parse(tms);
    }

    public static TinaReachability parseTinaReachability(String data) throws IOException, Parser.Exception {

        StringReader input = new StringReader(data);

        TinaReachabilityScanner trs = new TinaReachabilityScanner(input);
        TinaReachabilityParser trp = new TinaReachabilityParser();

        return (TinaReachability) trp.parse(trs);
    }

    public static void printTinaMarking(TinaMarking tm){

        for(NetMarking nm : tm.getNetMarkingList()){
            System.out.println("NetMarking: " + nm.getStateName());
            for(PlaceMarking pm : nm.getPlaceMarkingList()){
                System.out.println("--- PlaceMarking: " + pm.getPlaceName());

                if(pm.isMultMarking()){
                    System.out.println("----- Multiplier: " + pm.asMultMarking().getMultiplier());
                } else {
                    System.out.println("----- Multiplier: 1");
                }
            }
        }
    }

    public static void printTinaReachability(TinaReachability tr){

        for(NetReachability nr : tr.getNetReachabilityList()){
            System.out.println("NetReachability: " + nr.getTargetReachabilityStateName());
            for(EnabledTransition et : nr.getEnabledTransitionList()){
                System.out.println("--- EnabledTransition: " + et.getTransitionName() + "/" + et.getTargetReachabilityState());
            }
        }
    }
}
