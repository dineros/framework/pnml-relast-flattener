package de.tudresden.inf.st.pnml.flatter.template;

import de.tudresden.inf.st.pnml.jastadd.model.PetriNet;
import de.tudresden.inf.st.pnml.jastadd.model.PnObject;
import de.tudresden.inf.st.pnml.jastadd.model.PnmlParser;
import org.jetbrains.annotations.NotNull;

public class ServiceTemplates extends PnmlTemplate{

    public static PetriNet getServiceConnectionTemplate(String idSuffix){
        return getPetriNetTemplate(idSuffix, "ServiceConnectionTemplate.pnml");
    }

    public static PetriNet getServiceMultiplexerTemplate(String idSuffix){
        return getPetriNetTemplate(idSuffix, "ServiceMultiplexerTemplate.pnml");
    }

    public static PetriNet getServiceServerInterfaceTemplate(String idSuffix){
        return getPetriNetTemplate(idSuffix, "ServiceServerInterfaceTemplate.pnml");
    }

    @NotNull
    private static PetriNet getPetriNetTemplate(String idSuffix, String netPath) {
        PetriNet templateNet = PnmlParser.parsePnml(TEMPLATES_PATH + netPath, false).get(0);

        for (PnObject po : templateNet.allObjects()) {
            updatePnObjectIdAndName(po, idSuffix);
        }

        return templateNet;
    }
}

