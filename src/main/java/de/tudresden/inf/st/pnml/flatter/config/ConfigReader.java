package de.tudresden.inf.st.pnml.flatter.config;

import com.google.gson.Gson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.util.Scanner;

public class ConfigReader {

    private String configPath;

    protected static final Logger logger = LoggerFactory.getLogger(ConfigReader.class);

    public ConfigReader(String configPath) {
        this.configPath = configPath;
    }

    public String getConfigPath() {
        return configPath;
    }

    public void setConfigPath(String configPath) {
        this.configPath = configPath;
    }

    private String getConfigFileContent(){

        String data = "";

        try {
            File file = new File(this.configPath);
            Scanner scanner = new Scanner(file);

            while (scanner.hasNextLine()) {
                data += scanner.nextLine();
            }

            scanner.close();
        } catch (FileNotFoundException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }

        return data;
    }

    public String[] getTinaConfigParams(){

        Gson gson = new Gson();
        System.out.println("Content " + getConfigFileContent());
        FlatterConfig fc = gson.fromJson(getConfigFileContent(), FlatterConfig.class);
        return fc.tina.split(" ");
    }

    public String[] getPathtoConfigParams(){

        Gson gson = new Gson();
        FlatterConfig fc = gson.fromJson(getConfigFileContent(), FlatterConfig.class);
        return fc.pathto.split(" ");
    }

    public String[] getSiftConfigParams(){

        Gson gson = new Gson();
        FlatterConfig fc = gson.fromJson(getConfigFileContent(), FlatterConfig.class);
        return fc.sift.split(" ");
    }

    public String[] getStructConfigParams(){

        Gson gson = new Gson();
        FlatterConfig fc = gson.fromJson(getConfigFileContent(), FlatterConfig.class);
        return fc.struct.split(" ");
    }
}
