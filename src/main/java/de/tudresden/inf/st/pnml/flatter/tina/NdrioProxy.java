package de.tudresden.inf.st.pnml.flatter.tina;

import de.tudresden.inf.st.pnml.base.constants.PnmlConstants;
import de.tudresden.inf.st.pnml.jastadd.model.Arc;
import de.tudresden.inf.st.pnml.jastadd.model.PetriNet;
import de.tudresden.inf.st.pnml.jastadd.model.Transition;

import java.io.*;
import java.net.URISyntaxException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

public class NdrioProxy extends AbstractTinaProxy{

    public void pnml2net(String inputPath, String outputPath, String tinaPath, String tinaVersion) {

        ProcessBuilder ndrioProcessBuilder = new ProcessBuilder();

        if(!isWindows){

            Path currentPath;
            try {
                currentPath = Paths.get(NdrioProxy.class.getProtectionDomain()
                        .getCodeSource()
                        .getLocation()
                        .toURI());
            } catch (URISyntaxException e) {
                throw new RuntimeException(e);
            }

            // Get the parent directory (location of the JAR or class root)
            Path location = currentPath.getParent();
            String tinaSubPath = "/tina-" + tinaVersion + "/bin/ndrio";

            ndrioProcessBuilder.command(Objects.requireNonNullElseGet(tinaPath, location::toAbsolutePath) + tinaSubPath, inputPath, outputPath);
        }else{
            logger.error("Windows is currently not supported. Exiting ...");
            return;
        }

        try {

            File myObj = new File(outputPath);

            if (!myObj.exists()) {
                myObj.getParentFile().mkdirs();
            }

            Process process = ndrioProcessBuilder.start();

            int exitVal = process.waitFor();
            if (exitVal == 0) {
                return;
            }

        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     *
     * Includes inhibitor arcs which are not part of the .pnml format into the .net format. The inhibitor arc
     * information in .pnml is currently attached to arcs via ToolSpecifics in DiNeROS.
     *
     * Example of an inhibitor arc net within .net:
     *
     * net TestNet
     * tr {t1} {p3} {p1}?-1 -> {p2}
     *
     * pl {p1} (0)
     * pl {p2} (0)
     * pl {p3} (1)
     *
     * Where the curved brackets are escaping special signs within element names. Thus, these brackets are not always
     * part of the .net to be patched "patched".
     *
     * @param petriNet The flatted petri net.
     * @param inputPath The path of the flatted nets pnml.
     * @param outputPath The path to generate the resulting .net file to.
     */
    public void includeInhibitorArcs(PetriNet petriNet, String inputPath, String outputPath) {

        final String NET_INHIBITOR_ARC = "?-1";

        List<Arc> inhibitorArcs = new ArrayList<>();

        for(Arc a : petriNet.allArcs()){
            if(a.getNumToolspecific() > 0 && a.getType().equals(PnmlConstants.INHIBITOR_ARC)){
                System.out.println("[FLATTENER] Found inh arc: " + a.getId());
                inhibitorArcs.add(a);
            }
        }

        StringBuilder finalContent = new StringBuilder();

        try (BufferedReader br = new BufferedReader(new FileReader(inputPath))) {
            String line;
            while ((line = br.readLine()) != null) {
                // process line by line
                if(line.startsWith("tr")){

                    String[] splitLine = line.split(" ");
                    boolean matched = false;

                    for(Arc a : inhibitorArcs){
                        if(a.getTarget().getId().equals(splitLine[1].
                                replace("{", "").
                                replace("}", "").
                                replace(" ", ""))){

                            StringBuilder finalLineContent = new StringBuilder();
                            for(int i = 0; i < splitLine.length; i++){

                                if(a.getSource().getId().equals(splitLine[i].
                                        replace("{", "").
                                        replace("}", "").
                                        replace(" ", ""))){

                                    finalLineContent.append(" ");
                                    finalLineContent.append(splitLine[i]);
                                    finalLineContent.append(NET_INHIBITOR_ARC);

                                } else {
                                    if ( i != 0){
                                        finalLineContent.append(" ");
                                    }
                                    finalLineContent.append(splitLine[i]);
                                }
                            }

                            finalContent.append(finalLineContent);
                            finalContent.append("\n");
                            matched = true;
                            break;
                        }
                    }

                    if(!matched) {
                        finalContent.append(line);
                        finalContent.append("\n");
                    }
                }else {
                    finalContent.append(line);
                    finalContent.append("\n");
                }
            }

            BufferedWriter writer = new BufferedWriter(new FileWriter(new File(outputPath)));
            writer.write(finalContent.toString());
            writer.flush();
            writer.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
