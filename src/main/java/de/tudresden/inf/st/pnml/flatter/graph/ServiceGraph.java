package de.tudresden.inf.st.pnml.flatter.graph;

import de.tudresden.inf.st.pnml.jastadd.model.*;

import java.util.ArrayList;
import java.util.List;

public class ServiceGraph {

    public List<Node> graph;

    public static class Node{

        public String name;
        public String serviceName;
        public List<Node> targets;
        public List<Node> sources;

        public Node(String name, String serviceName, List<Node> targets, List<Node> sources) {
            this.name = name;
            this.serviceName = serviceName;
            this.targets = targets;
            this.sources = sources;
        }
    }

    public List<Node> init(PetriNet petriNet){

        graph = new ArrayList<>();

        // [1] Create a node for each service channel
        for(DinerosTransition t : petriNet.allDinerosTransitions()){
            if(t.getStaticTransitionInformation().isServiceTransitionInformation()){
                graph.add(new Node(t.getId(), t.getStaticTransitionInformation()
                        .asServiceTransitionInformation().getServiceName(), new ArrayList<>(), new ArrayList<>()));
            }
        }

        // [2] Iterate over nodes to build connections
        for(Node n : graph){
            for(DinerosTransition t : petriNet.allDinerosTransitions()){
                if(t.getId().equals(n.name)){
                    for(ServiceChannel sc : t.getStaticTransitionInformation().asServiceTransitionInformation().getClientChannels()){
                        for(Node iterNode : graph){
                            if(!n.name.equals(iterNode.name) &&
                                    isPnObjectInPageTree(petriNet.getTransitionById(iterNode.name).ContainingPage(), sc.getRequestPlaceId())){
                                // [3] Build connections
                                n.sources.add(iterNode);
                                iterNode.targets.add(n);
                            }
                        }
                    }
                }
            }
        }

        return graph;
    }

    public boolean isPotentiallyCyclic(String serviceName){

        for(Node n : graph){
            if(n.serviceName.equals(serviceName)){
                return isPotentiallyCyclicInternal(n, n);
            }
        }

        return true;
    }

    private boolean isPotentiallyCyclicInternal(Node node, Node iterNode){

        for(Node n : iterNode.targets){
            if(n.name.equals(node.name)){
                return true;
            }
        }

        for(Node n : iterNode.targets){
            if(isPotentiallyCyclicInternal(node, n)){
                return true;
            }
        }

        return false;
    }

    private boolean isPnObjectInPageTree(Page page, String id){

        // bfs
        for(PnObject po : page.getObjects()){
            if(po.getId().equals(id)){
                return true;
            }
        }

        for(PnObject po : page.getObjects()){
            if(po.isPageNode() &&
                    isPnObjectInPageTree(po.asPage(), id)){
                return true;
            }
        }

        return false;
    }
}
