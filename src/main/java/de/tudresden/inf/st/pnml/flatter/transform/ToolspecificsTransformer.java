package de.tudresden.inf.st.pnml.flatter.transform;

import de.tudresden.inf.st.pnml.base.constants.PnmlConstants;
import de.tudresden.inf.st.pnml.jastadd.model.*;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

public class ToolspecificsTransformer {

    /**
     * Updates service transitions toolspecifics after service instance flattening
     * serverInput/serverOutput remain unchanged because we know the new one are id + index + suffix
     * only client-wise channels are updated
     */
    public static void updateTransitionToolspecifics(PetriNet petriNet, int serviceCapacity, Page sPage) {

        Set<String> pagePlaceIds = new HashSet<>();

        for (PnObject p : sPage.getObjectList()) {
            if (p.isPlaceObject()) {
                pagePlaceIds.add(p.getId());
            }
        }

        for (DinerosTransition dt : petriNet.allDinerosTransitions()) {
            if (dt.getStaticTransitionInformation().isServiceTransitionInformation()) {

                if (dt.getMutableTransitionInformation() == null) {
                    dt.setMutableTransitionInformation(dt.getStaticTransitionInformation().asServiceTransitionInformation().treeCopyNoTransform());
                }

                for (ServiceChannel sc : dt.getMutableTransitionInformation().asServiceTransitionInformation().getClientChannelList()) {

                    if (pagePlaceIds.contains(sc.getRequestPlaceId().split("-")[0])
                            && pagePlaceIds.contains(sc.getResponsePlaceId().split("-")[0])) {

                        for (int i = 0; i < serviceCapacity; i++) {

                            ServiceChannel newSc = new ServiceChannel();
                            newSc.setId(UUID.randomUUID().toString());
                            newSc.setRequestPlaceId(sc.getRequestPlaceId() + "-" + i + "-" + PnmlConstants.PAGE_SERVER_INSTANCE_SUFFIX);
                            newSc.setResponsePlaceId(sc.getResponsePlaceId() + "-" + i + "-" + PnmlConstants.PAGE_SERVER_INSTANCE_SUFFIX);
                            dt.getMutableTransitionInformation().asServiceTransitionInformation().addClientChannel(newSc);
                        }

                        sc.removeSelf();
                        break;
                    }
                }
            }

            if (dt.getStaticTransitionInformation().isTopicTransitionInformation()) {

                if (dt.getMutableTransitionInformation() == null) {
                    dt.setMutableTransitionInformation(dt.getStaticTransitionInformation().asTopicTransitionInformation().treeCopyNoTransform());
                }

                for (PublisherPort pp : dt.getMutableTransitionInformation().asTopicTransitionInformation().getPublisherPorts()) {

                    if (pagePlaceIds.contains(pp.getPlaceId().split("-")[0])) {

                        for (int i = 0; i < serviceCapacity; i++) {

                            PublisherPort newPp = new PublisherPort();
                            newPp.setPlaceId(pp.getPlaceId() + "-" + i + "-" + PnmlConstants.PAGE_SERVER_INSTANCE_SUFFIX);
                            newPp.setLimit(pp.getLimit());
                            dt.getMutableTransitionInformation().asTopicTransitionInformation().addPublisherPort(newPp);
                        }

                        pp.removeSelf();
                        break;
                    }
                }
            }
        }
    }
}