package de.tudresden.inf.st.pnml.flatter.template;

import de.tudresden.inf.st.pnml.jastadd.model.*;

public class PrimitiveTemplates extends PnmlTemplate{

    public static DinerosTransition getDinerosTransition(){

        PetriNet templateNet = PnmlParser.parsePnml
                (ELEMENTS_PATH + "DinerosTransition.pnml", false).get(0);

        for(Transition t : templateNet.allTransitions()){
            if(t.getId().equals("DinerosTransition")){
                return t.asDinerosTransition();
            }
        }

        return null;
    }

    public static DinerosPlace getDinerosPlace(){

        PetriNet templateNet = PnmlParser.parsePnml
                (ELEMENTS_PATH + "DinerosPlace.pnml", false).get(0);

        for(Place p : templateNet.allPlaces()){
            if(p.getId().equals("DinerosPlace")){
                return p.asDinerosPlace();
            }
        }

        return null;
    }

    public static RefTransition getReferenceTransition(Transition target){

        PetriNet templateNet = PnmlParser.parsePnml
                (ELEMENTS_PATH + "RefTransition.pnml", false).get(0);

        for(RefTransition rt : templateNet.allRefTransitions()){
            rt.setRef(target);
            return rt;
        }

        return null;
    }

    public static RefPlace getReferencePlace(Place target){

        PetriNet templateNet = PnmlParser.parsePnml
                (ELEMENTS_PATH + "RefPlace.pnml", false).get(0);

        for(RefPlace rp : templateNet.allRefPlaces()){
            rp.setRef(target);
            return rp;
        }

        return null;
    }
}
