package de.tudresden.inf.st.pnml.flatter.transform;

import beaver.Parser;
import de.tudresden.inf.st.pnml.flatter.analysis.TinaUtils;
import de.tudresden.inf.st.pnml.flatter.graph.ServiceGraph;
import de.tudresden.inf.st.pnml.flatter.template.PrimitiveTemplates;
import de.tudresden.inf.st.pnml.flatter.template.TemplateConstants;
import de.tudresden.inf.st.pnml.flatter.tina.NdrioProxy;
import de.tudresden.inf.st.pnml.jastadd.model.*;
import fr.lip6.move.pnml.framework.utils.exception.InvalidIDException;

import java.io.IOException;
import java.util.*;

public class TransformationUtils {

    /**
     *
     * @param petriNet before signals are flatted
     * @return mapping from signal IDs to transitions using it
     */
    public static Map<java.lang.String, List<DinerosTransition>> getSignalToUsageMapping(PetriNet petriNet){

        Map<java.lang.String, List<DinerosTransition>> mapping = new HashMap<>();
        Map<java.lang.String, java.lang.String> signalDefinitions = petriNet.getInputSignalDefinition();

        // initialize the mapping for all signals
        for (Map.Entry<java.lang.String, java.lang.String> signalDef : signalDefinitions.entrySet()) {
            mapping.put(signalDef.getKey(), new ArrayList<>());
        }

        // insert signal using transitions into the mapping
        for (Map.Entry<java.lang.String, java.lang.String> signalDef : signalDefinitions.entrySet()) {
            for(DinerosTransition dt : petriNet.allDinerosTransitions()){
                if(dt.getStaticTransitionInformation().isSignalTransitionInformation()){
                    if(dt.getStaticTransitionInformation().
                            asSignalTransitionInformation().getClause().hasLiteral(signalDef.getKey())){
                        mapping.get(signalDef.getKey()).add(dt);
                    }
                }
            }
        }
        return mapping;
    }

    public static Arc createArc(String id, Node s, Node t) {

        Arc a = new Arc();
        a.setId(id);
        a.setSource(s);
        a.setTarget(t);

        Name n = new Name();
        n.setText(a.getId());
        a.setName(n);
        return a;
    }

    public static Arc createAndIncludeArc(Page page, Node s, Node t) {

        Arc a = new Arc();
        a.setId(s.getId() + "-to-" + t.getId());
        a.setSource(s);
        a.setTarget(t);

        Name n = new Name();
        n.setText(a.getId());
        a.setName(n);
        page.addObject(a);

        return a;
    }

    public static Arc createAndIncludeArc(Page page, String id, Node s, Node t) {

        Arc a = createArc(id, s, t);
        page.addObject(a);
        return a;
    }

    public static void createAndIncludeBidirectionalArc(Page page, Node s, Node t) {

        page.addObject(createArc(s.getId() + "-to-" + t.getId(), s, t));
        page.addObject(createArc(t.getId() + "-to-" + s.getId(), t, s));
    }

    public static Arc createAndIncludeInhibitorArc(Page page, String id, Node s, Node t) {

        Arc a = createArc(id, s, t);
        ToolInfo ti = new ToolInfo();
        String xml = "<toolspecific xmlns=\"http://www.pnml.org/version-2009/grammar/pnml\" tool=\"de.tudresden.inf.st.pnml.distributedPN\" version=\"0.1\">\n" +
                "                    <type>inhibitor</type>\n" +
                "                </toolspecific>";
        ti.setFormattedXMLBuffer(new StringBuffer(xml));
        ti.setTool("de.tudresden.inf.st.pnml.distributedPN");
        ti.setVersion("0.1");
        a.addToolspecific(ti);
        page.addObject(a);
        return a;
    }

    public static Map<String, PnObject> includeTemplateInstance(Page page, PetriNet petriNet, PetriNet templateInstance, Map<String, Node> refs, Map<String, PnObject> addedObjects) {
        return includeTemplateInstance(page, petriNet, templateInstance, "channel", "channel", refs, addedObjects);
    }

    public static Map<String, PnObject> includeTemplateInstance(Page page, PetriNet petriNet, PetriNet templateInstance, Map<String, PnObject> addedObjects) {
        Map<String, Node> refs = new HashMap<>();
        return includeTemplateInstance(page, petriNet, templateInstance, "channel", "channel", refs, addedObjects);
    }

    public static Map<String, PnObject> includeTemplateInstance(Page page, PetriNet petriNet, PetriNet templateInstance, String subnet, String node, Map<String, PnObject> addedObjects) {
        Map<String, Node> refs = new HashMap<>();
        return includeTemplateInstance(page, petriNet, templateInstance, subnet, node, refs, addedObjects);
    }

    public static Map<String, PnObject> includeTemplateInstance(Page page, PetriNet petriNet, PetriNet templateInstance, String subnet, String node, Map<String, Node> refs, Map<String, PnObject> addedObjects) {

        if(addedObjects == null){
            addedObjects = new HashMap<>();
        }

        for(RefTransition rt : templateInstance.allRefTransitions()){
            if(refs.containsKey(rt.getId())){
                rt.setRef(refs.get(rt.getId()).asTransitionNode());
            }
            page.addObject(rt);
            addedObjects.put(rt.getId(), rt);
        }

        for(RefPlace rp : templateInstance.allRefPlaces()){
            if(refs.containsKey(rp.getId())){
                rp.setRef(refs.get(rp.getId()).asPlaceNode());
            }
            page.addObject(rp);
            addedObjects.put(rp.getId(), rp);
        }

        // include places, transitions
        for (Transition t : templateInstance.allTransitions()) {

            SignalTransitionInformation newTi = new SignalTransitionInformation();
            newTi.setNode(node);
            newTi.setSubNet(subnet);
            t.asDinerosTransition().setMutableTransitionInformation(newTi);

            //System.out.println("Adding T: " + t.getId());
            page.addObject(t);
            addedObjects.put(t.getId(), t);
        }

        for (Place p : templateInstance.allPlaces()) {

            PlaceInformation newPi = new PlaceInformation();
            newPi.setNode(node);
            newPi.setSubNet(subnet);
            p.asDinerosPlace().setMutablePlaceInformation(newPi);

            //System.out.println("Adding P: " + p.getId());
            page.addObject(p);
            addedObjects.put(p.getId(), p);
        }

        petriNet.flushTreeCache();
        petriNet.flushCollectionCache();

        // connect elements by arcs
        for (Arc a : templateInstance.allArcs()) {
            Arc newArc = new Arc();
            newArc.setId(a.getId());
            //newArc.setId("arc-" + a.getSource().getId() + "-" + a.getTarget().getId());
            newArc.setTarget((Node) getPnObjectByID(templateInstance, a.getTarget().getId()));
            newArc.setSource((Node) getPnObjectByID(templateInstance, a.getSource().getId()));

            if (a.getNumToolspecific() > 0) {
                ToolInfo ti = new ToolInfo();
                ti.setFormattedXMLBuffer(a.getToolspecific(0).getFormattedXMLBuffer());
                ti.setTool(a.getToolspecific(0).getTool());
                ti.setVersion(a.getToolspecific(0).getVersion());
                newArc.addToolspecific(ti);
            }

            page.addObject(newArc);
            addedObjects.put(newArc.getId(), newArc);
        }

        petriNet.flushAttrAndCollectionCache();
        return addedObjects;
    }

    /**
     * @param page starting point
     * @param pages the filled set
     * @return
     */
    public static void getPagesRecursive(Page page, Set<Page> pages){

        pages.add(page);
        Set<Page> subPages = new HashSet<>();

        for(PnObject po : page.getObjectsNoTransform()){
            if(po.isPageNode()){
                subPages.add(po.asPage());
            }
        }

        if(subPages.size() == 0){
            return;
        }

        for(Page iterPage : subPages){
            getPagesRecursive(iterPage, pages);
        }
    }

    /**
     * @param page starting point
     * @param objects the filled map
     * @return
     */
    public static void getPnObjectsRecursive(Page page, Map<String, PnObject> objects){

        objects.put(page.getId(), page);
        Set<Page> subPages = new HashSet<>();

        for(PnObject po : page.getObjectsNoTransform()){
            objects.put(po.getId(), po);
            if(po.isPageNode()){
                subPages.add(po.asPage());
            }
        }

        if(subPages.size() == 0){
            return;
        }

        for(Page iterPage : subPages){
            getPnObjectsRecursive(iterPage, objects);
        }
    }

    private static DinerosTransition getTransitionByServiceName(PetriNet petriNet, String serviceName){

        for(DinerosTransition t : petriNet.allDinerosTransitions()){
            if(t.getStaticTransitionInformation().isServiceTransitionInformation()){
                if(t.getStaticTransitionInformation().
                        asServiceTransitionInformation().getServiceName().equals(serviceName)){
                    return t;
                }
            }
        }

        return null;
    }

    public static void transformPrototypePagesRecursive(Page page, Page parentPage,
                                                        PetriNet petriNet, ServiceGraph serviceGraph,
                                                        Map<String, PnObject> addedObjects, boolean flagOverrideInstanceCount){

        if(page.canTransformPrototypePage()){
            System.out.println("[INFO] Transforming page: " + page.getId());

            Page transformedPage;

            if(serviceGraph.isPotentiallyCyclic(page.getServiceName()) && !flagOverrideInstanceCount){
                System.out.println("[WARNING] Detected potentially cyclic service call on: " + page.getServiceName());
                System.out.println("[WARNING] Falling back to user defined thread number for: " + page.getServiceName());
                transformedPage = page.transformPrototypePage(petriNet,-1, addedObjects);
            } else if(!serviceGraph.isPotentiallyCyclic(page.getServiceName()) && !flagOverrideInstanceCount) {
                int numInstances = getTransitionByServiceName(petriNet,
                        page.getServiceName()).getStaticTransitionInformation().asServiceTransitionInformation().getNumClientChannel();
                transformedPage = page.transformPrototypePage(petriNet, numInstances, addedObjects);
                System.out.println("[INFO] Transforming prototype page: " + page.getServiceName() + " to " + numInstances + " instances.");
            } else {
                System.out.println("[WARNING] OverrideInstanceCount flag is used: " + page.getServiceName());
                System.out.println("[WARNING] Falling back to user defined thread number for: " + page.getServiceName());
                transformedPage = page.transformPrototypePage(petriNet,-1, addedObjects);
            }

            if(parentPage == null){
                for(int i = 0; i < petriNet.getNumPage(); i++){
                    if(petriNet.getPage(i).getId().equals(page.getId())){
                        petriNet.setPage(transformedPage, i);
                    }
                }
            } else {
                for(int i = 0; i < parentPage.getNumObject(); i++){
                    if(parentPage.getObject(i).getId().equals(page.getId())){
                        parentPage.setObject(transformedPage, i);
                    }
                }
            }
        }

        Set<Page> subPages = new HashSet<>();
        for(PnObject po : page.getObjectsNoTransform()){
            if(po.isPageNode()){
                subPages.add(po.asPage());
            }
        }

        if(subPages.size() == 0){
            return;
        }

        for(Page iterPage : subPages){
            transformPrototypePagesRecursive(iterPage, page, petriNet, serviceGraph, addedObjects, flagOverrideInstanceCount);
        }
    }

    public static Transition getTransitionByID(PetriNet petriNet, String Id) {

        for (Transition t : petriNet.allTransitions()) {
            if (t.getId().equals(Id)) {
                return t;
            }
        }

        return null;
    }

    public static PnObject getPnObjectByID(PetriNet petriNet, String Id) {

        for (PnObject po : petriNet.allObjects()) {
            if (po.getId().equals(Id)) {
                return po;
            }
        }

        return null;
    }

    public static PetriNet sliceOutPage(String pnmlPath, Page page){

        PetriNet petriNet = PnmlParser.parsePnml(pnmlPath, false).get(0);
        List<PnObject> objectsToDelete = new ArrayList<>();

        for(PnObject po : petriNet.getPage(0).getObjects()){
            if(!po.getId().equals(page.getId())){
                objectsToDelete.add(po);
            }
        }

        for(PnObject po : objectsToDelete){
            System.out.println("Deleting: " + po.getId());
            po.removeSelf();
        }

        petriNet.flushTreeCache();
        return petriNet;
    }

    public static List<Page> getSourceNodePages(PetriNet petriNet){

        List<Page> pages = new ArrayList<>();

        for(Page page : petriNet.allPages()){
            for(ToolInfo ti : page.getToolspecifics()){
                if(ti.getFormattedXMLBuffer().indexOf("nodePage") != -1){

                    // check if source node
                    Set<String> placesInPage = new HashSet<>();
                    placesInPage(page, placesInPage);

                    boolean isPublisher = false;
                    boolean isSubscriber = false;
                    boolean isServer = false;
                    boolean isClient = false;

                    for(DinerosTransition t : petriNet.allDinerosTransitions()){
                        if(t.getStaticTransitionInformation().isServiceTransitionInformation()){
                            if(placesInPage.contains(t.getStaticTransitionInformation().
                                    asServiceTransitionInformation().getServerChannel().getRequestPlaceId())){
                                isServer = true;
                                break;
                            }

                            for(ServiceChannel sc : t.getStaticTransitionInformation()
                                    .asServiceTransitionInformation().getClientChannels()){
                                if(placesInPage.contains(sc.getRequestPlaceId())){
                                    isClient = true;
                                    break;
                                }
                            }
                        }

                        if(t.getStaticTransitionInformation().isTopicTransitionInformation()){
                            for(SubscriberPort sp : t.getStaticTransitionInformation().
                                    asTopicTransitionInformation().getSubscriberPorts()){
                                if(placesInPage.contains(sp.getPlaceId())){
                                    isSubscriber = true;
                                    break;
                                }
                            }

                            for(PublisherPort pp : t.getStaticTransitionInformation().
                                    asTopicTransitionInformation().getPublisherPorts()){
                                if(placesInPage.contains(pp.getPlaceId())){
                                    isPublisher = true;
                                    break;
                                }
                            }
                        }
                    }

                    if(isPublisher && !isSubscriber && !isClient && !isServer){
                        System.out.println("Found source page: " + page.getId());
                        pages.add(page);
                    }

                    break;
                }
            }
        }
        return pages;
    }

    public static Map<String, Integer> getPlaceToBoundMapping(String pnmlPath, PetriNet petriNet, List<Page> sourceNodePages,
                                                              String tinaPath, String tinaVersion)
            throws InvalidIDException, IOException, Parser.Exception {

        Map<String, Integer> mapping = new HashMap<>();

        for(Page sourcePage : sourceNodePages){

            Set<String> pagePlaces = new HashSet<>();
            placesInPage(sourcePage, pagePlaces);

            for(String pid : pagePlaces){
                for(DinerosTransition dt : petriNet.allDinerosTransitions()){
                    if(dt.getStaticTransitionInformation().isTopicTransitionInformation()){
                        for(PublisherPort pp : dt.getStaticTransitionInformation().asTopicTransitionInformation().getPublisherPorts()){
                            if (pp.getPlaceId().equals(pid)){

                                System.out.println("Computing bound for place: " + pid);
                                String sliceId = UUID.randomUUID().toString();
                                PetriNet pnSlice = TransformationUtils.sliceOutPage(pnmlPath, sourcePage);

                                for(Page p : pnSlice.allPages()){
                                    if(p.getId().equals("top")){
                                        ReferenceFlatter.flatReferencesAndPages(pnSlice, p);
                                        break;
                                    }
                                }

                                String pnmlExportPath = PnmlExporter.serializeToPnmlFile(pnSlice, "-slice-" + sliceId);
                                NdrioProxy ndrioProxy = new NdrioProxy();
                                String ndrioTargetPath = System.getProperty("user.dir") + "/temp/net/" + sliceId + ".net";
                                ndrioProxy.pnml2net(pnmlExportPath, ndrioTargetPath, tinaPath, tinaVersion);

                                int bound = TinaUtils.checkBoundednessOfPlace(pid, ndrioTargetPath, sourcePage.getId());
                                System.out.println("Adding new bound mapping: " + pid + " >> " + bound);
                                mapping.put(pid, bound);
                            }
                        }
                    }
                }
            }
        }

        return mapping;
    }

    private static void placesInPage(Page page, Set<String> places){

        for(PnObject po : page.getObjects()){
            if(po.isPlaceObject()){
                places.add(po.getId());
            }
            if(po.isPageNode()){
                placesInPage(po.asPage(), places);
            }
        }
    }

    public static void createNewSignalLinkage(Page page, Transition toTrue, Transition toFalse,
                                              Place placeTrue, Place placeFalse, int usageCount,
                                              Place p, String suffix, boolean usageCountFlag) {
        if (!p.getId().startsWith(TemplateConstants.PD_PLACE_PREFIX)) {
            if (usageCount == 0 && usageCountFlag) {
                System.out.println("[SignalPostProcessing] Creating new linkage between " + p.getId() + " and " + toTrue.getId());
                TransformationUtils.createAndIncludeBidirectionalArc(page, p, toTrue);

                System.out.println("[SignalPostProcessing] Creating new linkage between " + p.getId() + " and " + toFalse.getId());
                TransformationUtils.createAndIncludeBidirectionalArc(page, p, toFalse);
            }

            if(usageCount > 0 || !usageCountFlag){
                DinerosTransition newToTrue = PrimitiveTemplates.getDinerosTransition();
                long millis = System.currentTimeMillis();
                newToTrue.setId(toTrue.getId() + "-" + usageCount + "-" + millis + suffix);
                newToTrue.getName().setText(newToTrue.getId());
                DinerosTransition newToFalse = PrimitiveTemplates.getDinerosTransition();
                newToFalse.setId(toFalse.getId() + "-" + usageCount + "-" + millis + suffix);
                newToFalse.getName().setText(newToFalse.getId());
                page.addObject(newToTrue);
                page.addObject(newToFalse);

                for(Place tip : toTrue.incomingPlaces()){
                    if (tip.getId().startsWith(TemplateConstants.PD_PLACE_PREFIX)) {
                        System.out.println("Relink 1: " + newToTrue.getId());
                        TransformationUtils.createAndIncludeArc(page, tip, newToTrue);
                    }
                }

                for(Place top : toTrue.outgoingPlaces()){
                    if (top.getId().startsWith(TemplateConstants.PD_PLACE_PREFIX)) {
                        System.out.println("Relink 2: " + newToTrue.getId());
                        TransformationUtils.createAndIncludeArc(page, newToTrue, top);
                    }
                }

                for(Place tip : toFalse.incomingPlaces()){
                    if (tip.getId().startsWith(TemplateConstants.PD_PLACE_PREFIX)) {
                        System.out.println("Relink 3: " + newToFalse.getId());
                        TransformationUtils.createAndIncludeArc(page, tip, newToFalse);
                    }
                }

                for(Place top : toFalse.outgoingPlaces()){
                    if (top.getId().startsWith(TemplateConstants.PD_PLACE_PREFIX)) {
                        System.out.println("Relink 4: " + newToFalse.getId());
                        TransformationUtils.createAndIncludeArc(page, newToFalse, top);
                    }
                }

                TransformationUtils.createAndIncludeArc(page, placeTrue, newToFalse);
                TransformationUtils.createAndIncludeArc(page, newToFalse, placeFalse);
                TransformationUtils.createAndIncludeArc(page, placeFalse, newToTrue);
                TransformationUtils.createAndIncludeArc(page, newToTrue, placeTrue);

                System.out.println("[SignalPostProcessing] Creating new linkage between " + p.getId() + " and " + newToTrue.getId());
                TransformationUtils.createAndIncludeBidirectionalArc(page, p, newToTrue);

                System.out.println("[SignalPostProcessing] Creating new linkage between " + p.getId() + " and " + newToFalse.getId());
                TransformationUtils.createAndIncludeBidirectionalArc(page, p, newToFalse);
            }
        }
    }
}
