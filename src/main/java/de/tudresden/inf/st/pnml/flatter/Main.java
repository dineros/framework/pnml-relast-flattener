package de.tudresden.inf.st.pnml.flatter;

import beaver.Parser;
import de.tudresden.inf.st.pnml.base.constants.PnmlConstants;
import de.tudresden.inf.st.pnml.flatter.graph.ServiceGraph;
import de.tudresden.inf.st.pnml.flatter.transform.ReferenceFlatter;
import de.tudresden.inf.st.pnml.flatter.transform.TransformationUtils;
import de.tudresden.inf.st.pnml.jastadd.model.*;
import de.tudresden.inf.st.pnml.jastadd.model.PnmlParser;
import fr.lip6.move.pnml.framework.utils.exception.InvalidIDException;

import de.tudresden.inf.st.pnml.flatter.tina.NdrioProxy;
import de.tudresden.inf.st.pnml.jastadd.model.PetriNet;

import java.io.IOException;
import java.util.*;

public class Main {

    public static void main(String[] args) throws InvalidIDException, InterruptedException, IOException, Parser.Exception {

        // Display help if no arguments or `--help` is passed
        if (args.length == 0 || Arrays.asList(args).contains("--help")) {
            printUsage();
            return;
        }


        // Parse arguments with clear flags
        boolean flagInhibitor = Boolean.parseBoolean(getArgumentValue(args, "--inhibitor", "false"));
        boolean flagSignal = Boolean.parseBoolean(getArgumentValue(args, "--signal", "false"));
        boolean flagPurge = Boolean.parseBoolean(getArgumentValue(args, "--purge", "false"));
        boolean flagOverrideInstanceCount = Boolean.parseBoolean(getArgumentValue(args, "--override", "false"));
        String tinaPath = getArgumentValue(args, "--tinaPath", null);
        String tinaVersion = getArgumentValue(args, "--tinaVersion", "3.7.0");

        // String configPath = getArgumentValue(args, "--config", null);
        String pnmlPath = getArgumentValue(args, "--pnml", null);

        // Validate required arguments
        if (pnmlPath == null) {
            System.err.println("Error: --pnml <path> is required.");
            printUsage();
            System.exit(1);
        }

        // Print parsed arguments (for debugging or confirmation)
        System.out.println("Parsed Arguments:");
        System.out.println("  flagInhibitor: " + flagInhibitor);
        System.out.println("  flagSignal: " + flagSignal);
        System.out.println("  flagPurge: " + flagPurge);
        System.out.println("  flagOverrideInstanceCount: " + flagOverrideInstanceCount);
        // System.out.println("  configPath: " + configPath);
        System.out.println("  pnmlPath: " + pnmlPath);
        System.out.println("  tinaPath: " + tinaPath);
        System.out.println("  tinaVersion: " + tinaVersion);

        // Application logic here (replace this with your actual logic)
        System.out.println("Application started successfully.");

        boolean flagOutArcs = false;
        boolean flagNoSignal = false;

        doFlattening(pnmlPath, flagInhibitor, flagSignal, flagPurge, flagOverrideInstanceCount, flagOutArcs, flagNoSignal, tinaPath, tinaVersion);
        // doFlattening("", "/home/sebastian/git/sesac-exercise/tools/model-top.pnml", false, false, false, false, false, false, null);

    }


    // Utility to extract argument value by flag
    private static String getArgumentValue(String[] args, String flag, String defaultValue) {
        for (int i = 0; i < args.length; i++) {
            if (args[i].equals(flag) && i + 1 < args.length) {
                return args[i + 1];
            }
        }
        return defaultValue;
    }

    // Print help/usage instructions
    private static void printUsage() {
        System.out.println("Options:");
        System.out.println("  --inhibitor <true|false>          Enable or disable inhibitor flag (default: false)");
        System.out.println("  --signal <true|false>             Enable or disable signal flag (default: false)");
        System.out.println("  --purge <true|false>              Enable or disable purge flag (default: false)");
        System.out.println("  --override <true|false>           Enable or disable override instance count flag (default: false)");
        System.out.println("  --config <path>                   Path to the configuration file (optional)");
        System.out.println("  --tinaPath <path>                 Path to the tina installation (default: local)");
        System.out.println("  --tinaVersion <String>            Version of the tina installation (default: 3.7.0)");
        System.out.println("  --pnml <path>                     Path to the PNML file (required)");
        System.out.println("  --help                            Show this help message and exit");
    }

    private static void doFlattening(String pnmlPath, boolean flagInhibitor,
                                     boolean flagSignal, boolean flagPurge, boolean flagOverrideInstanceCount,
                                     boolean flagOutArcs, boolean flagNoSignal, String tinaPath, String tinaVersion)
            throws InvalidIDException, IOException, InterruptedException, Parser.Exception {

        if (pnmlPath == null) {
            System.out.println("[ERROR] No model found on given input path.");
            return;
        }

     //   if (configPath == null || configPath.isEmpty()) {
    //        System.out.println("[WARNING] No config path configured, no model checking will be executed.");
    //    }

        // parse the global not flatted petri net
        PetriNet petriNet = PnmlParser.parsePnml(pnmlPath, true).get(0);
        Map<String, Integer> bounds = null;

        if (flagPurge) {
            bounds = TransformationUtils.getPlaceToBoundMapping
                    (pnmlPath, petriNet, TransformationUtils.getSourceNodePages(petriNet), tinaPath, tinaVersion);
        }
        // [STAGE 1] Resolve service prototype pages
        ServiceGraph serviceGraph = new ServiceGraph();
        serviceGraph.init(petriNet);

        Map<String, PnObject> addedInstanceObjects = new HashMap<>();
        TransformationUtils.transformPrototypePagesRecursive(petriNet.getPage(0), null,
                petriNet, serviceGraph, addedInstanceObjects, flagOverrideInstanceCount);
        petriNet.flushAttrAndCollectionCache();

        // [STAGE 2] Transform topic transitions
        for (DinerosTransition dt : petriNet.allDinerosTransitions()) {
            if (dt.canTransformTopicTransition()) {
                Page topicTransformedPage = dt.transformTopicElement(bounds, flagInhibitor, flagPurge);
                for (int i = 0; i < dt.ContainingPage().getNumObject(); i++) {
                    if (dt.ContainingPage().getObject(i).getId().equals(dt.getId())) {
                        dt.ContainingPage().setObject(topicTransformedPage, i);
                    }
                }
                petriNet.flushTreeCache();
            }
        }

        petriNet.flushTreeCache();

        // [STAGE 3] Transform service transitions
        for (DinerosTransition dt : petriNet.allDinerosTransitions()) {
            if (dt.canTransformServiceTransition()) {
                Page serviceTransformedPage = dt.transformServiceElement(addedInstanceObjects);
                for (int i = 0; i < dt.ContainingPage().getNumObject(); i++) {
                    if (dt.ContainingPage().getObject(i).getId().equals(dt.getId())) {
                        dt.ContainingPage().setObject(serviceTransformedPage, i);
                    }
                }
                petriNet.flushTreeCache();
            }
        }

        if (!flagNoSignal) {
            // [STAGE 4] Transform signals
            Page signalValuePage = new Page();
            signalValuePage.setId(PnmlConstants.SIGNAL_VALUE_PAGE_ID);
            petriNet.addPage(signalValuePage);
            petriNet.transformSignalElements(signalValuePage);
            petriNet.flushTreeCache();
            petriNet.flushAttrAndCollectionCache();

            for (DinerosTransition dt : petriNet.allDinerosTransitions()) {
                Page dtClausePage = dt.getStaticTransitionInformation().asSignalTransitionInformation()
                        .getClause().transformClauseElement(dt, petriNet);
                petriNet.addPage(dtClausePage);
            }

            petriNet.flushTreeCache();
        }

        // [STAGE 5] make sure that we have a valid marking
        System.out.println("[FLATTENER] Checking marking.");
        for (Place p : petriNet.allPlaces()) {
            if (p.getInitialMarking() == null) {
                System.out.println("[FLATTER] Found NULL-marking. Falling back to 0 ...");
                PTMarking marking = new PTMarking();
                marking.setText(0);
                p.setInitialMarking(marking);
            }
        }

        // [STAGE 6] remove references / pages
        System.out.println("[FLATTENER] Breaking references and pages.");
        for (Page p : petriNet.allPages()) {
            if (p.getId().equals("top")) {
                ReferenceFlatter.flatReferencesAndPages(petriNet, p);
                break;
            }
        }

        // [STAGE 7] Postprocessing
        System.out.println("[FLATTENER] Running postprocessing.");
        if (flagSignal) {
            petriNet.transformSingleUseSignals(flagOutArcs);
            petriNet.flushAttrAndCollectionCache();
            petriNet.flushTreeCache();
        }

        printNet(petriNet, false, false);

        // [STAGE 8] export flatted net to pnml
        System.out.println("[FLATTENER] Exporting to pnml.");
        String exportId = UUID.randomUUID().toString();
        String pnmlExportPath = PnmlExporter.serializeToPnmlFile(petriNet, "-flatted-" + exportId);

        //  convert and add inhibitor arcs
        System.out.println("[FLATTENER] Converting to net format.");
        NdrioProxy ndrioProxy = new NdrioProxy();
        String homeDirectory = System.getProperty("user.dir");
        String fid = UUID.randomUUID().toString();
        String ndrioTargetPath = homeDirectory + "/temp/net/" + fid + "-flatted-" + exportId + ".net";

        // resolve inhibitor arcs
        System.out.println("[FLATTENER] Including inhibitor arcs into net format.");
        String inhibitorTargetPath;

        inhibitorTargetPath = Objects.requireNonNullElseGet(null, () -> homeDirectory + "/temp/net/" + fid + "-flatted-inh-" + exportId + ".net");

        ndrioProxy.pnml2net(pnmlExportPath, ndrioTargetPath, tinaPath, tinaVersion);
        ndrioProxy.includeInhibitorArcs(petriNet, ndrioTargetPath, inhibitorTargetPath);

        // [STAGE 9] Analyze
        // read config for analyzer from file
     /*   if (configPath != null && !configPath.isEmpty() && !configPath.equals("-nc")) {
            ConfigReader cr = new ConfigReader(configPath);
            String[] tinaConfig = cr.getTinaConfigParams();
            String[] siftConfig = cr.getSiftConfigParams();

            // insert into tina
            if (tinaConfig.length > 1) {
                System.out.println("[FLATTENER] Inserting into tina.");
                TinaProxy tinaProxy = new TinaProxy();
                String tinaTargetPath = homeDirectory + "/temp/tina/" + "tina-result-" + exportId + ".txt";
                tinaProxy.analyzePetriNet(inhibitorTargetPath, tinaTargetPath, tinaConfig);
            }

            // insert into sift
            if (siftConfig.length > 1) {
                System.out.println("[FLATTENER] Inserting into sift.");
                SiftProxy siftProxy = new SiftProxy();
                String siftTargetPath = homeDirectory + "/temp/sift/" + "sift-result-" + exportId + ".ktz";
                siftProxy.analyzePetriNet(inhibitorTargetPath, siftTargetPath, siftConfig);

                System.out.println("[FLATTENER] Converting with ktzio.");
                KtzioProxy ktzioProxy = new KtzioProxy();
                String ktzioPath = homeDirectory + "/temp/sift/" + "sift-result-converted-" + exportId + ".txt";
                ktzioProxy.convertBinaryToText(siftTargetPath, ktzioPath);
            }
        }*/

        System.out.println("[FLATTENER] Finished.");
    }

    public static void printNet(PetriNet petriNet, boolean withArcs, boolean withToolSpecifics) {

        System.out.println("--------------- STRUCTURE ---------------");
        System.out.println("----------------- PLACES ----------------");

        for (Place p : petriNet.allPlaces()) {
            System.out.println("Place " + p.asDinerosPlace().getId() + " -- " + p.asDinerosPlace().getStaticPlaceInformation().getSubNet());
            if (p.getInitialMarking() != null) {
                System.out.println("--- Marking: " + p.getInitialMarking().getText());
            } else {
                System.out.println("--- Marking: NULL");
            }
        }

        System.out.println("-------------- TRANSITIONS --------------");

        for (Transition t : petriNet.allTransitions()) {
            System.out.println("Transition " + t.getId());
        }

        System.out.println("-------------- TRANSITION DETAILS --------------");

        for (Transition t : petriNet.allTransitions()) {

            if (t.asDinerosTransition().getStaticTransitionInformation().isServiceTransitionInformation()) {
                System.out.println("--- Transition: " + t.getId() + " subnet: " + t.asDinerosTransition().getStaticTransitionInformation().getSubNet()
                        + " service: " + t.asDinerosTransition().getStaticTransitionInformation().asServiceTransitionInformation().getServiceName() + " ---------");
            } else if (t.asDinerosTransition().getStaticTransitionInformation().isTopicTransitionInformation()) {
                System.out.println("--- Transition: " + t.getId() + " subnet: " + t.asDinerosTransition().getStaticTransitionInformation().getSubNet()
                        + " topic: " + t.asDinerosTransition().getStaticTransitionInformation().asTopicTransitionInformation().getTopic() + " ---------");
            } else {
                System.out.println("--- Transition: " + t.getId() + " subnet: " + t.asDinerosTransition().getStaticTransitionInformation().getSubNet() + " --- name: " + t.getName().getText());
            }

            for (Place p : t.asDinerosTransition().incomingPlaces()) {

                System.out.println("------ Inputplace:  " + p.getId() + " subnet: " + p.asDinerosPlace().getStaticPlaceInformation().getSubNet() + " ---------");
            }

            for (Place p : t.asDinerosTransition().outgoingPlaces()) {

                System.out.println("------ Outputplace: " + p.getId() + " subnet: " + p.asDinerosPlace().getStaticPlaceInformation().getSubNet() + " ---------");
            }
        }

        System.out.println("----------------- REF PLACES -----------------");

        for (RefPlace rp : petriNet.allRefPlaces()) {
            System.out.println("--- RefPlace: " + rp.getId() + " (" + rp.getName().getText() + ") >> " + rp.getRef().getId());
        }

        System.out.println("----------------- REF TRANSITIONS -----------------");

        for (RefTransition rt : petriNet.allRefTransitions()) {
            System.out.println("--- RefTransition: " + rt.getId() + " >> " + rt.getRef().getId());
        }

        if (withArcs) {
            System.out.println("----------------- ARCS -----------------");

            for (Arc a : petriNet.allArcs()) {
                System.out.println("Arc: " + a.getId() + " -- source: " + a.getSource().getId() + " -- target: " + a.getTarget().getId());
            }
        }


        System.out.println("--------------- T SIGNALS (STATIC)---------------");

        for (Transition t : petriNet.allTransitions()) {
            DinerosTransition ist = t.asDinerosTransition();

            if (ist != null && ist.getMutableTransitionInformation() == null) {
                if (ist.getStaticTransitionInformation().isSignalTransitionInformation()) {
                    System.out.println(ist.getStaticTransitionInformation().asSignalTransitionInformation().getClause().printClause());
                }
            }
        }

        if (withToolSpecifics) {
            System.out.println("--------------- TOOL SPECIFIC ---------------");

            for (Transition t : petriNet.allTransitions()) {
                DinerosTransition ist = t.asDinerosTransition();
                if (ist != null && ist.getNumToolspecific() > 0) {
                    System.out.println("ToolSpecific: (" + ist.getName().getText() + ") " + ist.getToolspecific(0).getFormattedXMLBuffer().toString());
                }
            }
        }
    }
}