package de.tudresden.inf.st.pnml.flatter.template;

import de.tudresden.inf.st.pnml.jastadd.model.PetriNet;
import de.tudresden.inf.st.pnml.jastadd.model.PnObject;
import de.tudresden.inf.st.pnml.jastadd.model.PnmlParser;

public class SignalTemplates extends PnmlTemplate{

    public static PetriNet getInputSignalTemplate(String idSuffix){

        PetriNet templateNet = PnmlParser.parsePnml
                (TEMPLATES_PATH + "InputSignal.pnml", false).get(0);

        for(PnObject po : templateNet.allObjects()){
            updatePnObjectIdAndName(po, idSuffix);
        }

        return templateNet;
    }
}
