package de.tudresden.inf.st.pnml.flatter.tina;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class SiftProxy extends AbstractTinaProxy {

    public void analyzePetriNet(String inputPath, String outputPath, String[] config) throws IOException, InterruptedException {

        ProcessBuilder siftProcessBuilder = new ProcessBuilder();

        if (!isWindows) {

            File file = new File(outputPath);
            if (!file.exists()) {
                file.getParentFile().mkdirs();
            }

            List<String> commandParts = new ArrayList<>();
            if (homeDirectory.contains("pnml-relast-flatter")) {

                commandParts.add(homeDirectory + "/libs/tina-3.7.0/bin/sift");
            } else {

                commandParts.add(homeDirectory + "/pnml-relast-flatter/libs/tina-3.7.0/bin/sift");
            }
            commandParts.addAll(Arrays.asList(config));
            commandParts.add(inputPath);
            commandParts.add(outputPath);

            siftProcessBuilder.command(commandParts);
            Process p = siftProcessBuilder.start();
            p.waitFor();
        } else {
            logger.error("Windows is currently not supported. Exiting ...");
            return;
        }
    }
}
