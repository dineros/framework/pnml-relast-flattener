package de.tudresden.inf.st.pnml.flatter.transform;

import de.tudresden.inf.st.pnml.jastadd.model.*;

import java.util.HashSet;
import java.util.Set;

public class ReferenceFlatter {

    private static void findPlaceReferences(PlaceNode placeNode, Set<PlaceNode> nodes){

        nodes.addAll(placeNode.getReferencingPlaces());

        for(RefPlace rp : placeNode.getReferencingPlaces()){
            findPlaceReferences(rp, nodes);
        }
    }

    private static void findTransitionReferences(TransitionNode transitionNode, Set<TransitionNode> nodes){

        nodes.addAll(transitionNode.getReferencingTransitions());

        for(RefTransition rt : transitionNode.getReferencingTransitions()){
            findTransitionReferences(rt, nodes);
        }
    }

    public static void flatReferencesAndPages(PetriNet petriNet, Page page){

        excludeAllPages(petriNet, page);
        resolvePlaceReferences(petriNet);
        resolveTransitionReferences(petriNet);
        petriNet.flushTreeCache();
        petriNet.flushAttrAndCollectionCache();
    }

    private static void resolvePlaceReferences(PetriNet petriNet){

        Set<PlaceNode> nodesToDelete = new HashSet<>();

        for(Place p : petriNet.allReferencedPlaces()){

            Set<PlaceNode> referencer = new HashSet<>();
            Set<Arc> connectedInArcs = new HashSet<>();
            Set<Arc> connectedOutArcs = new HashSet<>();

            findPlaceReferences(p, referencer);

            // get all arcs
            for(PlaceNode placeNode : referencer){
                connectedInArcs.addAll(placeNode.getInArcs());
                connectedOutArcs.addAll(placeNode.getOutArcs());
                nodesToDelete.add(placeNode);
            }

            // relink in-arcs
            for(Arc inArc: connectedInArcs){
                inArc.setTarget(p);
            }

            // relink out-arcs
            for(Arc outArc: connectedOutArcs){
                outArc.setSource(p);
            }
        }

        for(PlaceNode placeNode : nodesToDelete){
            placeNode.removeSelf();
        }
    }

    private static void resolveTransitionReferences(PetriNet petriNet){

        Set<TransitionNode> nodesToDelete = new HashSet<>();

        for(Transition t : petriNet.allReferencedTransitions()){

            System.out.println("Resolving reference on : " + t. getId());

            Set<TransitionNode> referencer = new HashSet<>();
            Set<Arc> connectedInArcs = new HashSet<>();
            Set<Arc> connectedOutArcs = new HashSet<>();

            findTransitionReferences(t, referencer);

            // get all arcs
            for(TransitionNode transitionNode : referencer){
                System.out.println(">> Detected reference: " + transitionNode.getId());
                connectedInArcs.addAll(transitionNode.getInArcs());
                connectedOutArcs.addAll(transitionNode.getOutArcs());
                nodesToDelete.add(transitionNode);
            }

            // relink in-arcs
            for(Arc inArc: connectedInArcs){
                inArc.setTarget(t);
            }

            // relink out-arcs
            for(Arc outArc: connectedOutArcs){
                outArc.setSource(t);
            }
        }

        for(TransitionNode transitionNode : nodesToDelete){
            transitionNode.removeSelf();
        }
    }

    /**
     * Excludes all but the top level page.
     * @param petriNet
     */
    private static void excludeAllPages(PetriNet petriNet, Page targetPage){

        Page topLevelPage = null;

        for (Page p : petriNet.allPages()){
            if(p.getId().equals("top")){
                topLevelPage = p;
            }
        }

        for (PnObject pnObject : petriNet.allObjects()){
            assert topLevelPage != null;
            if(pnObject.ContainingPage() != null && !pnObject.isPageNode()){
                topLevelPage.addObject(pnObject);
            }
        }

        for (Page p : petriNet.allPages()){
            if(!p.getId().equals(targetPage.getId())){
                p.removeSelf();
            }
        }
    }
}
