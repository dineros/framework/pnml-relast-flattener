package de.tudresden.inf.st.pnml.flatter.tina;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class TinaProxy extends AbstractTinaProxy{

    public void analyzePetriNet(String inputPath, String outputPath, String[] config) throws IOException {

        ProcessBuilder tinaProcessBuilder = new ProcessBuilder();

        File file = new File(outputPath);
        if (!file.exists()) {
            file.getParentFile().mkdirs();
        }
        file.createNewFile();

        FileWriter fileWriter = new FileWriter(outputPath);

        if (!isWindows) {

            List<String> commandParts = new ArrayList<>();
            if (homeDirectory.contains("pnml-relast-flattener")) {

                commandParts.add(homeDirectory + "/libs/tina-3.7.0/bin/tina");
            } else {

                commandParts.add(homeDirectory + "/pnml-relast-flattener/libs/tina-3.7.0/bin/tina");
            }
            commandParts.addAll(Arrays.asList(config));
            commandParts.add(inputPath);
            tinaProcessBuilder.command(commandParts);
        } else {
            logger.error("Windows is currently not supported. Exiting ...");
            return;
        }

        executeAndRead(tinaProcessBuilder, fileWriter);
    }
}
