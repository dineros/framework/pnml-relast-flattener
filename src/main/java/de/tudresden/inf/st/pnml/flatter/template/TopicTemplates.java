package de.tudresden.inf.st.pnml.flatter.template;

import de.tudresden.inf.st.pnml.flatter.transform.TransformationUtils;
import de.tudresden.inf.st.pnml.jastadd.model.*;

public class TopicTemplates extends PnmlTemplate {

    public static final String INH_REPLACE_ARC = "inh-replace-arc-";
    public static final String INH_REPLACE_REV_ARC = "inh-replace-rev-arc-";

    public static PetriNet getTopicPublisherPetriNet(String idSuffix, int capacity, boolean withoutInh){

        if(withoutInh){
            return getTopicPublisherOptPetriNet(idSuffix, capacity);
        }

        PetriNet templateNet = PnmlParser.parsePnml(TEMPLATES_PATH + "TopicPublisherTemplate.pnml", false).get(0);
        initTemplate(idSuffix, capacity, templateNet,
                TemplateConstants.PUBLISHER_CAPACITY_PLACE, TemplateConstants.PUBLISHER_TEMPLATE_PAGE);
        return templateNet;
    }

    public static PetriNet getFlatTopicPublisherPetriNet(String idSuffix){

        PetriNet templateNet = PnmlParser.parsePnml(TEMPLATES_PATH + "TopicPublisherTemplateFlat.pnml", false).get(0);
        updateTemplateNaming(idSuffix, templateNet, TemplateConstants.PUBLISHER_TEMPLATE_PAGE);
        return templateNet;
    }

    public static PetriNet getFlatTopicDispatcherPetriNet(String idSuffix){

        PetriNet templateNet = PnmlParser.parsePnml(TEMPLATES_PATH + "TopicSubDispatcherTemplateFlat.pnml", false).get(0);
        updateTemplateNaming(idSuffix, templateNet, TemplateConstants.DISPATCHER_TEMPLATE_PAGE);
        return templateNet;
    }

    public static PetriNet getFlatTopicCallbackPetriNet(String idSuffix){

        PetriNet templateNet = PnmlParser.parsePnml(TEMPLATES_PATH + "TopicSubCallbackQueueTemplateFlat.pnml", false).get(0);
        updateTemplateNaming(idSuffix, templateNet, TemplateConstants.CALLBACK_QUEUE_TEMPLATE_PAGE);
        return templateNet;
    }

    private static PetriNet getTopicPublisherOptPetriNet(String idSuffix, int capacity){

        PetriNet templateNet = PnmlParser.parsePnml(TEMPLATES_PATH + "TopicPublisherTemplateOpt.pnml", false).get(0);
        addInhibitorReplacementArcs(capacity, templateNet, TemplateConstants.PUBLISHER_CONNECTOR_PLACE,
                TemplateConstants.PUBLISHER_OVERFLOW_TRANSITION, "pub-");
        initTemplate(idSuffix, capacity, templateNet,
                TemplateConstants.PUBLISHER_CAPACITY_PLACE, TemplateConstants.PUBLISHER_TEMPLATE_PAGE);
        return templateNet;
    }

    public static PetriNet getTopicDispatcherPetriNet(String idSuffix, int capacity, boolean withoutInh){

        if(withoutInh){
            return getTopicDispatcherOptPetriNet(idSuffix, capacity);
        }

        PetriNet templateNet = PnmlParser.parsePnml(TEMPLATES_PATH + "TopicSubDispatcherTemplate.pnml", false).get(0);
        initTemplate(idSuffix, capacity, templateNet, TemplateConstants.DISPATCHER_CAPACITY_PLACE,
                TemplateConstants.DISPATCHER_TEMPLATE_PAGE);
        return templateNet;
    }

    public static PetriNet getTopicDispatcherOptPetriNet(String idSuffix, int capacity){

        PetriNet templateNet = PnmlParser.parsePnml(TEMPLATES_PATH + "TopicSubDispatcherTemplateOpt.pnml", false).get(0);
        addInhibitorReplacementArcs(capacity, templateNet, TemplateConstants.DISPATCHER_CONNECTOR_PLACE,
                TemplateConstants.DISPATCHER_OVERFLOW_TRANSITION, "dis-");
        initTemplate(idSuffix, capacity, templateNet, TemplateConstants.DISPATCHER_CAPACITY_PLACE,
                TemplateConstants.DISPATCHER_TEMPLATE_PAGE);
        return templateNet;
    }

    public static PetriNet getTopicCallbackQueuePetriNet(String idSuffix, int capacity, boolean withoutInh){

        if(withoutInh){
            return getTopicCallbackQueueOptPetriNet(idSuffix, capacity);
        }

        PetriNet templateNet = PnmlParser.parsePnml(TEMPLATES_PATH + "TopicSubCallbackQueueTemplate.pnml", false).get(0);
        initTemplate(idSuffix, capacity, templateNet, TemplateConstants.CALLBACK_CAPACITY_PLACE,
                TemplateConstants.CALLBACK_QUEUE_TEMPLATE_PAGE);
        return templateNet;
    }

    public static PetriNet getTopicCallbackQueueOptPetriNet(String idSuffix, int capacity){

        PetriNet templateNet = PnmlParser.parsePnml(TEMPLATES_PATH + "TopicSubCallbackQueueTemplateOpt.pnml", false).get(0);
        addInhibitorReplacementArcs(capacity, templateNet, TemplateConstants.CALLBACK_CONNECTOR_PLACE,
                TemplateConstants.CALLBACK_OVERFLOW_TRANSITION, "callback-");
        initTemplate(idSuffix, capacity, templateNet, TemplateConstants.CALLBACK_CAPACITY_PLACE,
                TemplateConstants.CALLBACK_QUEUE_TEMPLATE_PAGE);
        return templateNet;
    }

    private static void initTemplate(String idSuffix, int capacity, PetriNet templateNet,
                                     String capacityPlace, String templatePage) {
        for (Place p : templateNet.allPlaces()) {
            if (p.getId().equals(capacityPlace)) {
                p.getInitialMarking().setText(capacity);
            }
        }

        updateTemplateNaming(idSuffix, templateNet, templatePage);
    }

    private static void updateTemplateNaming(String idSuffix, PetriNet templateNet, String templatePage) {
        for (PnObject po : templateNet.allObjects()) {
            if (!po.getId().equals(templatePage)) {
                updatePnObjectIdAndName(po, idSuffix);
            }
        }
    }

    private static void addInhibitorReplacementArcs(int capacity, PetriNet templateNet,
                                                    String connectorPlace, String overflowTransition, String templatePrefix) {
        for (int i = 0; i < capacity; i++) {
            TransformationUtils.createAndIncludeArc(templateNet.getPage(0), templatePrefix + INH_REPLACE_ARC + i,
                    templateNet.getPlaceById(connectorPlace),
                    templateNet.getTransitionById(overflowTransition));
            TransformationUtils.createAndIncludeArc(templateNet.getPage(0), templatePrefix + INH_REPLACE_REV_ARC + i,
                    templateNet.getTransitionById(overflowTransition),
                    templateNet.getPlaceById(connectorPlace));
        }

        templateNet.flushTreeCache();
    }
}
