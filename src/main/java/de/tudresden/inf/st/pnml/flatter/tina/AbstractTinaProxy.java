package de.tudresden.inf.st.pnml.flatter.tina;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;

public abstract class AbstractTinaProxy {

    protected static final Logger logger = LoggerFactory.getLogger(AbstractTinaProxy.class);

    protected boolean isWindows = System.getProperty("os.name").toLowerCase().startsWith("windows");
    protected String homeDirectory = System.getProperty("user.dir");

    protected void executeAndRead(ProcessBuilder tinaProcessBuilder, FileWriter fileWriter) {

        try {
            Process process = tinaProcessBuilder.start();
            BufferedReader reader = new BufferedReader(
                    new InputStreamReader(process.getInputStream()));

            String line;
            while ((line = reader.readLine()) != null) {
                fileWriter.write(line + "\n");
            }

            fileWriter.close();

            int exitVal = process.waitFor();
            if (exitVal != 0) {
                System.exit(0);
            }

        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
    }

}
